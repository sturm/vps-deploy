# Things to do

* Speed up deploy chmod
* Currently you can't deploy if your default push repository is unavailable
* Switch to pathlib
* Separate code from collected static files, environment variables and
  instance-specific settings
* Allow deploying a specific revision
* Take a look at features provide by Kamal
